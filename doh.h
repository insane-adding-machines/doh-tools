#ifndef __DOH
#define __DOH

#define VERSION     "v0.1"
#define DNS_PORT    53
#define DOH_PORT    8053
#define BUFSZ       4096

#define IP6_LOCALHOST { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1 }
#define DOH_RPL_MIN 40
#define DOHD_REQ_MIN 40

#define STR_ACCEPT_DNS_MSG "Accept: application/dns-message"
#define STR_CONTENT_TYPE_DNS_MSG "Content-Type: application/dns-message"
#define STR_CONTENT_LEN    "Content-Length: "
#define STR_DOH_REPLY "HTTP/1.1 200 OK\r\nServer: dohd\r\nContent-Type: application/dns-message\r\nContent-Length: %d\r\nCache-Control: max-age=%d\r\n\r\n"
#define STR_DOH_REQUEST "POST / HTTP/1.1\r\nAccept: application/dns-message\r\nContent-Type: application/dns-message\r\nContent-Length: %d\r\n\r\n"
#define DNSQ_SUFFIX_LEN (4)

struct __attribute__((packed)) dns_header
{
    uint16_t id;        /* Packet id */
    uint8_t rd : 1;     /* Recursion Desired */
    uint8_t tc : 1;     /* TrunCation */
    uint8_t aa : 1;     /* Authoritative Answer */
    uint8_t opcode : 4; /* Opcode */
    uint8_t qr : 1;     /* Query/Response */
    uint8_t rcode : 4;  /* Response code */
    uint8_t z : 3;      /* Zero */
    uint8_t ra : 1;     /* Recursion Available */
    uint16_t qdcount;   /* Question count */
    uint16_t ancount;   /* Answer count */
    uint16_t nscount;   /* Authority count */
    uint16_t arcount;   /* Additional count */
};

struct client_data {
    WOLFSSL *ssl;
    struct evquick_event *ev_doh, *ev_dns;
    int tls_handshake_done;
    uint16_t id;
    int doh_sd;
    int dns_sd;
    struct sockaddr_in dns_client;
    struct client_data *next;
};

#endif
