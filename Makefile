CC?=gcc
#CFLAGS=-ggdb

WOLFSSL_INC:=$(shell pkg-config --cflags wolfssl)
WOLFSSL_LIB:=$(shell pkg-config --libs wolfssl)

CFLAGS=-O3 -Wall -Wextra -Wno-sign-compare $(WOLFSSL_INC)
LDFLAGS=$(WOLFSSL_LIB) -lwolfssl -lrt

all: dohd dohc

dohd: dohd.o libevquick.o
	gcc -o $@ $^ $(LDFLAGS)

dohc: dohc.o libevquick.o
	gcc -o $@ $^ $(LDFLAGS)

clean:
	rm -f *.o dohd dohc
